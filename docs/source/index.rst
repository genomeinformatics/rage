.. ddrage documentation master file, created by
   sphinx-quickstart on Mon Jun  6 11:52:14 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. include:: ../../README.rst


.. toctree::
   :maxdepth: 1

   getting-started/index
   documentation/index
   changelog
   faqs

* :ref:`search`

