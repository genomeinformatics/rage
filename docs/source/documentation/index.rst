*************
Documentation
*************

This documentation contains a list of :doc:`assumption and notation conventions <assumptions_and_notation>` used by ddRAGE.
File specifications for :doc:`input files <input_format>` and :doc:`output files <output_format>`, and a :doc:`detailed parameter list <parameters>` are also available.
The documentation for :doc:`tools <tools>` (found in the ``ddrage/tools`` folder) providing functions that surpass the simulation, can be found here as well.

..
   To modify the source code of ddRAGE or gain insight into the inner workings of the tool you can take a look at the :doc:`module documentation <autodoc/modules>`.


.. toctree::
   :maxdepth: 1

   assumptions_and_notation
   input_format
   output_format
   parameters
   enzymes
   tools
